﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    public float speed = 4.0f;        // metres per second
    public float turnSpeed = 180.0f;  // degrees per second
    public GameObject player1, player2;
    public Transform target;
    public Vector2 heading = Vector3.right;
        // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public ParticleSystem explosionPrefab;

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
    }


    void Start() {
        // find the player to set the target
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;

        player1 = GameObject.Find("Player");
        player2 = GameObject.Find("Player2");

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
                                 Random.value);
    }
    void Update()
    {
        Vector2 distance1 = player1.transform.position - this.gameObject.transform.position;
        Vector2 distance2 = player2.transform.position - this.gameObject.transform.position;


        if (distance1.magnitude < distance2.magnitude)
        {
            target = player1.transform;
        }
        else
        {
            target = player2.transform;
        }

        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }


}
