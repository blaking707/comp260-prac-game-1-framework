﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float maxSpeed = 5.0f;
    Vector2 direction;
    private beespawn beeSpawner;
    float destroyRadius = 1f;

    void Start()
    {
        beeSpawner = FindObjectOfType<beespawn>();
    }


    void Update()
    {
        // get the input values
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(transform.position, destroyRadius);
        }

        if (this.gameObject.name == "Player")
        {
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
        }
        else if (this.gameObject.name == "Player2")
        {
            direction.x = Input.GetAxis("Horizontal2");
            direction.y = Input.GetAxis("Vertical2");
        }
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}
